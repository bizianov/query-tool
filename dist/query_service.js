'use strict';

System.register(['./queries'], function (_export, _context) {
  "use strict";

  var getEventsListWithPlayers, getAllSensorDataByPlayerNameInTimeRange, getAllSensorDataByTeamNameInTimeRange, getPlayerAverageDateInTimeRange, getTeamAverageDateInTimeRange, getPlayerSumDataInTimeRange, getTeamSumDataInTimeRange, _createClass, QueryService;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  return {
    setters: [function (_queries) {
      getEventsListWithPlayers = _queries.getEventsListWithPlayers;
      getAllSensorDataByPlayerNameInTimeRange = _queries.getAllSensorDataByPlayerNameInTimeRange;
      getAllSensorDataByTeamNameInTimeRange = _queries.getAllSensorDataByTeamNameInTimeRange;
      getPlayerAverageDateInTimeRange = _queries.getPlayerAverageDateInTimeRange;
      getTeamAverageDateInTimeRange = _queries.getTeamAverageDateInTimeRange;
      getPlayerSumDataInTimeRange = _queries.getPlayerSumDataInTimeRange;
      getTeamSumDataInTimeRange = _queries.getTeamSumDataInTimeRange;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export('QueryService', QueryService = function () {
        function QueryService($http, url, username, password) {
          _classCallCheck(this, QueryService);

          this.$http = $http;
          this.url = url;
          this.username = username;
          this.password = password;
          this.queryMapping = {
            0: getAllSensorDataByPlayerNameInTimeRange,
            1: getAllSensorDataByTeamNameInTimeRange,
            2: getPlayerAverageDateInTimeRange,
            3: getTeamAverageDateInTimeRange,
            6: getPlayerSumDataInTimeRange,
            7: getTeamSumDataInTimeRange,
            100: getEventsListWithPlayers
          };
        }

        _createClass(QueryService, [{
          key: 'executeQuery',
          value: function executeQuery(query, variables) {
            console.log('Variables: ', variables);
            var url = 'http://localhost:1337/' + this.url;
            var convertedAuth = window.btoa(this.username + ':' + this.password).toString('base64');
            return this.$http({
              method: 'POST',
              url: url,
              data: JSON.stringify({
                query: this.queryMapping[query.code],
                variables: variables
              }),
              headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + convertedAuth
              }
            }).then(function (response) {
              console.log('Response: ', response);
              return response;
            });
          }
        }, {
          key: 'getEventsListWithPlayers',
          value: function getEventsListWithPlayers(variables) {
            return this.executeQuery({ name: 'Events list with players', code: 100 }, variables);
          }
        }]);

        return QueryService;
      }());

      _export('QueryService', QueryService);
    }
  };
});
//# sourceMappingURL=query_service.js.map
