'use strict';

System.register(['app/plugins/sdk', './query_service', './csv_service', 'lodash', './css/query-panel.css!', './utils', './queries'], function (_export, _context) {
  "use strict";

  var PanelCtrl, QueryService, CsvService, _, calculateMinStartAndMaxEnd, distinctTeams, distinctPlayers, normalizePlayerResponse, normalizeTeamResponse, normalizePlayerAverageResponse, normalizeTeamAverageResponse, normalizePlayerComparisonResponse, normalizeTeamComparisonResponse, singlePlayerComment, singleTeamComment, singlePlayerAvgComment, singleTeamAvgComment, twoPlayersComment, twoTeamsComment, _createClass, panelDefaults, QueryCtrl;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && (typeof call === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  return {
    setters: [function (_appPluginsSdk) {
      PanelCtrl = _appPluginsSdk.PanelCtrl;
    }, function (_query_service) {
      QueryService = _query_service.QueryService;
    }, function (_csv_service) {
      CsvService = _csv_service.CsvService;
    }, function (_lodash) {
      _ = _lodash.default;
    }, function (_cssQueryPanelCss) {}, function (_utils) {
      calculateMinStartAndMaxEnd = _utils.calculateMinStartAndMaxEnd;
      distinctTeams = _utils.distinctTeams;
      distinctPlayers = _utils.distinctPlayers;
      normalizePlayerResponse = _utils.normalizePlayerResponse;
      normalizeTeamResponse = _utils.normalizeTeamResponse;
      normalizePlayerAverageResponse = _utils.normalizePlayerAverageResponse;
      normalizeTeamAverageResponse = _utils.normalizeTeamAverageResponse;
      normalizePlayerComparisonResponse = _utils.normalizePlayerComparisonResponse;
      normalizeTeamComparisonResponse = _utils.normalizeTeamComparisonResponse;
    }, function (_queries) {
      singlePlayerComment = _queries.singlePlayerComment;
      singleTeamComment = _queries.singleTeamComment;
      singlePlayerAvgComment = _queries.singlePlayerAvgComment;
      singleTeamAvgComment = _queries.singleTeamAvgComment;
      twoPlayersComment = _queries.twoPlayersComment;
      twoTeamsComment = _queries.twoTeamsComment;
    }],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      panelDefaults = {
        projects: [{ name: 'Euroleague', code: 0 }],
        currentProject: {},
        events: [],
        currentEvents: [],
        queries: [{ name: 'Sensor data by player and type', code: 0, comment: singlePlayerComment }, { name: 'Sensor data by team and type', code: 1, comment: singleTeamComment }, { name: 'Average sensor data by player', code: 2, comment: singlePlayerAvgComment }, { name: 'Average sensor data by team', code: 3, comment: singleTeamAvgComment }, { name: 'Compare 2 players', code: 6, comment: twoPlayersComment }, { name: 'Compare 2 teams', code: 7, comment: twoTeamsComment }],
        currentQuery: {},
        playerQueryCodes: [0, 2, 6],
        teamQueryCodes: [1, 3, 7],
        teamAvgQueryCodes: [3, 7],
        comparisonQueryCodes: [6, 7],
        playerMetrics: [{ name: 'Jumps', code: 0 }, { name: 'Heartrates', code: 1 }, { name: 'Steps', code: 2 }, { name: 'Energy', code: 3 }, { name: 'Pressures', code: 4 }, { name: 'Explosions', code: 5 }],
        currentPlayerMetrics: [],
        players: [],
        currentPlayers: [],
        teamMetrics: [{ name: 'Noise level', code: 0 }, { name: 'Fan attitudes', code: 1 }, { name: 'Audio classes', code: 2 }],
        teamAvgMetrics: [{ name: 'Jumps', code: 3 }, { name: 'Energy', code: 4 }, { name: 'Fan attitudes', code: 5 }, { name: 'Fan peak attitudes', code: 6 }],
        currentTeamMetrics: [],
        teams: [],
        currentTeams: [],
        timeRange: {},
        customTimeRange: {},
        resultPrecision: 10,
        showPlayerTable: false,
        showTeamTable: false,
        noDataFoundAlert: false,
        notEnoughFilters: false,
        data: [],
        loadingData: false,
        csvDelimiters: ['Comma ,', 'Pipe |'],
        csvDelimiter: ',',
        // TODO: should be empty by default
        url: 'heedprod.heedplatform.iotarndgermany.de:38443/graphql',
        username: 'guest',
        password: 'cv%6xYFPaev!PLU5'
      };

      _export('QueryCtrl', QueryCtrl = function (_PanelCtrl) {
        _inherits(QueryCtrl, _PanelCtrl);

        function QueryCtrl($scope, $injector, $http) {
          _classCallCheck(this, QueryCtrl);

          var _this = _possibleConstructorReturn(this, (QueryCtrl.__proto__ || Object.getPrototypeOf(QueryCtrl)).call(this, $scope, $injector, $http));

          _.defaultsDeep(_this.panel, panelDefaults);
          _this.events.on('panel-initialized', _this.render.bind(_this));
          _this.events.on('init-edit-mode', _this.onInitEditMode.bind(_this));
          _this.$http = $http;
          // TODO: remove after clear username/password
          _this.queryService = new QueryService(_this.$http, _this.panel.url, _this.panel.username, _this.panel.password);
          return _this;
        }

        _createClass(QueryCtrl, [{
          key: 'onInitEditMode',
          value: function onInitEditMode() {
            this.addEditorTab('Options', 'public/plugins/grafana-query-panel/editor.html', 2);
          }
        }, {
          key: 'saveCredentials',
          value: function saveCredentials() {
            this.queryService = new QueryService(this.$http, this.panel.username, this.panel.password);
          }
        }, {
          key: 'projectChanged',
          value: function projectChanged() {
            var panel = this.panel;
            console.log('Current project: ' + panel.currentProject.name);
            var variables = {};
            this.queryService.getEventsListWithPlayers(variables).then(function (response) {
              panel.events = response.data.data.sports.games.map(function (event) {
                return {
                  id: event.id,
                  name: event.shortName,
                  duration: event.duration,
                  homeTeam: event.homeTeam,
                  awayTeam: event.awayTeam
                };
              });
            });
          }
        }, {
          key: 'eventChanged',
          value: function eventChanged() {
            this.resetFilters();
            var events = this.panel.currentEvents;
            console.log('Current events: ', events);
            this.panel.timeRange = calculateMinStartAndMaxEnd(events);
            this.panel.customTimeRange = {
              startTime: this.panel.timeRange.startTime,
              endTime: this.panel.timeRange.endTime
            };
            var teams = [];
            events.forEach(function (event) {
              teams.push(event.homeTeam, event.awayTeam);
            });
            this.panel.teams = distinctTeams(teams);
            var players = [];
            events.forEach(function (event) {
              players = players.concat(event.homeTeam.players);
              players = players.concat(event.awayTeam.players);
            });
            this.panel.players = distinctPlayers(players);
          }
        }, {
          key: 'queryChanged',
          value: function queryChanged() {
            console.log('Current query: ', this.panel.currentQuery.name);
            this.resetFilters();
          }
        }, {
          key: 'showTableContent',
          value: function showTableContent() {
            var _this2 = this;

            this.panel.loadingData = true;
            this.panel.noDataFoundAlert = false;
            this.panel.notEnoughFilters = false;
            this.panel.showPlayerTable = false;
            this.panel.showTeamTable = false;

            var variables = {};
            var playersProcessed = 0;
            var teamsProcessed = 0;
            this.panel.data = [];

            if (this.panel.currentPlayerMetrics.length * this.panel.currentPlayers.length > 0) {
              var currentPlayerMetricsNames = this.panel.currentPlayerMetrics.map(function (metric) {
                return metric.name;
              });
              var playersToBeProcessed = this.slicePlayersToBeProcessed();
              playersToBeProcessed.forEach(function (player) {
                variables = _this2.preparePlayerVariables(player, currentPlayerMetricsNames);
                _this2.queryService.executeQuery(_this2.panel.currentQuery, variables).then(function (response) {
                  _this2.panel.data = _this2.panel.data.concat(_this2.normalizeResponse(response.data.data.sports.players));
                  playersProcessed++;
                  if (playersToBeProcessed.length === playersProcessed) {
                    _this2.panel.data = _this2.prepareComparePlayersView();
                    _this2.panel.showPlayerTable = true;
                    _this2.panel.noDataFoundAlert = true;
                    _this2.panel.loadingData = false;
                  }
                });
              });
            } else if (this.panel.currentTeamMetrics.length * this.panel.currentTeams.length > 0) {
              var currentTeamMetricsNames = this.panel.currentTeamMetrics.map(function (metric) {
                return metric.name;
              });
              var teamsToBeProcessed = this.sliceTeamsToBeProcessed();
              teamsToBeProcessed.forEach(function (team) {
                variables = _this2.prepareTeamVariables(team, currentTeamMetricsNames);
                _this2.queryService.executeQuery(_this2.panel.currentQuery, variables).then(function (response) {
                  _this2.panel.data = _this2.panel.data.concat(_this2.normalizeResponse(response.data.data.sports.teams));
                  teamsProcessed++;
                  if (teamsToBeProcessed.length === teamsProcessed) {
                    console.log(_this2.panel.data);
                    _this2.panel.data = _this2.prepareCompareTeamsView();
                    _this2.panel.showTeamTable = true;
                    _this2.panel.noDataFoundAlert = true;
                    _this2.panel.loadingData = false;
                  }
                });
              });
            } else {
              this.panel.notEnoughFilters = true;
              this.panel.loadingData = false;
            }
          }
        }, {
          key: 'slicePlayersToBeProcessed',
          value: function slicePlayersToBeProcessed() {
            var playersToBeProcessed = [];
            if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
              playersToBeProcessed = this.panel.currentPlayers.slice(0, 2);
            } else {
              playersToBeProcessed = this.panel.currentPlayers.slice(0, 1);
            }
            return playersToBeProcessed;
          }
        }, {
          key: 'sliceTeamsToBeProcessed',
          value: function sliceTeamsToBeProcessed() {
            var teamsToBeProcessed = [];
            if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
              teamsToBeProcessed = this.panel.currentTeams.slice(0, 2);
            } else {
              teamsToBeProcessed = this.panel.currentTeams.slice(0, 1);
            }
            return teamsToBeProcessed;
          }
        }, {
          key: 'prepareComparePlayersView',
          value: function prepareComparePlayersView() {
            var compareViewResult = [];
            if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
              var data = this.panel.data;
              for (var index = 0; index < data.length / 2; index++) {
                compareViewResult.push({
                  player: data[index].player + ' / ' + data[index + data.length / 2].player,
                  metric: data[index].metric,
                  value: data[index].value + ' / ' + data[index + data.length / 2].value
                });
              }
            } else {
              compareViewResult = this.panel.data;
            }
            return compareViewResult;
          }
        }, {
          key: 'prepareCompareTeamsView',
          value: function prepareCompareTeamsView() {
            var compareViewResult = [];
            if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
              var data = this.panel.data;
              for (var index = 0; index < data.length / 2; index++) {
                compareViewResult.push({
                  team: data[index].team + ' / ' + data[index + data.length / 2].team,
                  metric: data[index].metric,
                  value: data[index].value + ' / ' + data[index + data.length / 2].value
                });
              }
            } else {
              compareViewResult = this.panel.data;
            }
            return compareViewResult;
          }
        }, {
          key: 'normalizeResponse',
          value: function normalizeResponse(response) {
            switch (this.panel.currentQuery.code) {
              case 0:
                return normalizePlayerResponse(response);
              case 1:
                return normalizeTeamResponse(response);
              case 2:
                return normalizePlayerAverageResponse(response);
              case 3:
                return normalizeTeamAverageResponse(response);
              case 6:
                return normalizePlayerComparisonResponse(response);
              case 7:
                return normalizeTeamComparisonResponse(response);
              default:
                return [];
            }
          }
        }, {
          key: 'preparePlayerVariables',
          value: function preparePlayerVariables(player, playerMetricsNames) {
            return {
              playerName: player.name,
              resultAmount: this.panel.resultPrecision,
              startActionTime: this.panel.customTimeRange.startTime,
              endActionTime: this.panel.customTimeRange.endTime,
              includeJumps: playerMetricsNames.indexOf('Jumps') > -1,
              includeSteps: playerMetricsNames.indexOf('Steps') > -1,
              includeEnergy: playerMetricsNames.indexOf('Energy') > -1,
              includePressures: playerMetricsNames.indexOf('Pressures') > -1,
              includeHeartrates: playerMetricsNames.indexOf('Heartrates') > -1,
              includeExplosions: playerMetricsNames.indexOf('Explosions') > -1
            };
          }
        }, {
          key: 'prepareTeamVariables',
          value: function prepareTeamVariables(team, currentTeamMetricsNames) {
            var variables = {};
            // TODO: consider if it's possible not to be bind on metrics name (metric.code is better)
            if (this.panel.teamMetrics.map(function (metric) {
              return metric.name;
            }).indexOf(currentTeamMetricsNames[0]) > -1) {
              variables = {
                teamName: team.name,
                resultAmount: this.panel.resultPrecision,
                startActionTime: this.panel.customTimeRange.startTime,
                endActionTime: this.panel.customTimeRange.endTime,
                includeNoiseLevels: currentTeamMetricsNames.indexOf('Noise level') > -1,
                includeFanAttitudes: currentTeamMetricsNames.indexOf('Fan attitudes') > -1,
                includeAudioClasses: currentTeamMetricsNames.indexOf('Audio classes') > -1
              };
            } else {
              variables = {
                teamName: team.name,
                resultAmount: this.panel.resultPrecision,
                startActionTime: this.panel.customTimeRange.startTime,
                endActionTime: this.panel.customTimeRange.endTime,
                includeJumps: currentTeamMetricsNames.indexOf('Jumps') > -1,
                includeEnergy: currentTeamMetricsNames.indexOf('Energy') > -1,
                includeFanAttitudes: currentTeamMetricsNames.indexOf('Fan attitudes') > -1,
                includeFanPeakAttitudes: currentTeamMetricsNames.indexOf('Fan peak attitudes') > -1
              };
            }
            return variables;
          }
        }, {
          key: 'resetFilters',
          value: function resetFilters() {
            this.panel.currentTeamMetrics = [];
            this.panel.currentTeams = [];
            this.panel.currentPlayerMetrics = [];
            this.panel.currentPlayers = [];
            this.panel.data = [];
            this.panel.noDataFoundAlert = false;
            this.panel.notEnoughFilters = false;
          }
        }, {
          key: 'resetTimeToDefault',
          value: function resetTimeToDefault() {
            this.panel.customTimeRange = {
              startTime: this.panel.timeRange.startTime,
              endTime: this.panel.timeRange.endTime
            };
          }
        }, {
          key: 'playerMetricsChanged',
          value: function playerMetricsChanged() {
            console.log('Current actions: ', this.panel.currentPlayerMetrics);
          }
        }, {
          key: 'teamMetricsChanges',
          value: function teamMetricsChanges() {
            console.log('Current team metrics: ', this.panel.currentTeamMetrics);
          }
        }, {
          key: 'playerChanged',
          value: function playerChanged() {
            console.log('Current players: ', this.panel.currentPlayers);
          }
        }, {
          key: 'teamChanged',
          value: function teamChanged() {
            console.log('Current teams: ', this.panel.currentTeams);
          }
        }, {
          key: 'copyTableContentAsTable',
          value: function copyTableContentAsTable() {
            var csvService = new CsvService(this.panel.csvDelimiter);
            var fileName = this.generateCsvFileName();
            csvService.exportTableToCSV(fileName + '.csv');
          }
        }, {
          key: 'generateCsvFileName',
          value: function generateCsvFileName() {
            var event = this.panel.currentEvents.map(function (_event) {
              return _event.name;
            });
            var queryCode = this.panel.currentQuery.name.split(' ')[0];
            var time = this.panel.customTimeRange;
            var metrics = [];
            var playersOrTeams = [];
            if (this.panel.playerQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
              metrics = metrics.concat(this.panel.currentPlayerMetrics.map(function (metric) {
                return metric.name;
              }));
              if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
                playersOrTeams = playersOrTeams.concat([this.panel.currentPlayers[0].name, this.panel.currentPlayers[1].name]);
              } else {
                playersOrTeams.push(this.panel.currentPlayers[0].name);
              }
            } else {
              metrics = metrics.concat(this.panel.currentTeamMetrics.map(function (metric) {
                return metric.name;
              }));
              if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
                playersOrTeams = playersOrTeams.concat([this.panel.currentTeams[0].name, this.panel.currentTeams[1].name]);
              } else {
                playersOrTeams.push(this.panel.currentTeams[0].name);
              }
            }
            return event + '-' + queryCode + '-' + metrics + '-' + playersOrTeams + '-' + time.startTime + '-' + time.endTime;
          }
        }]);

        return QueryCtrl;
      }(PanelCtrl));

      _export('QueryCtrl', QueryCtrl);

      QueryCtrl.templateUrl = 'module.html';
    }
  };
});
//# sourceMappingURL=query_ctrl.js.map
