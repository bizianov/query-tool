'use strict';

System.register(['./query_ctrl'], function (_export, _context) {
    "use strict";

    var QueryCtrl;
    return {
        setters: [function (_query_ctrl) {
            QueryCtrl = _query_ctrl.QueryCtrl;
        }],
        execute: function () {
            _export('PanelCtrl', QueryCtrl);
        }
    };
});
//# sourceMappingURL=module.js.map
