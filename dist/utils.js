'use strict';

System.register([], function (_export, _context) {
  "use strict";

  var calculateMinStartAndMaxEnd, distinctTeams, distinctPlayers, normalizePlayerResponse, normalizeJump, normalizeStep, normalizeTeamResponse, normalizeFanAttitude, normalizeAudioClass, normalizePlayerAverageResponse, normalizeAvgJump, normalizeAvgStep, normalizeTeamAverageResponse, normalizeAgvFanAttitude, normalizePlayerComparisonResponse, normalizeTeamComparisonResponse, normalizeSumFanAttitude, normalizeSumJump;
  return {
    setters: [],
    execute: function () {
      _export('calculateMinStartAndMaxEnd', calculateMinStartAndMaxEnd = function calculateMinStartAndMaxEnd(events) {
        var minStart = events.map(function (event) {
          return event.duration.start;
        }).sort().shift();
        var maxEnd = events.map(function (event) {
          return event.duration.end;
        }).sort().pop();
        return {
          startTime: minStart,
          endTime: maxEnd
        };
      });

      _export('distinctTeams', distinctTeams = function distinctTeams(teams) {
        var sortedTeams = teams.sort(function (team1, team2) {
          return team1.name.localeCompare(team2.name);
        });
        for (var index = 0; index < sortedTeams.length - 1; index++) {
          if (sortedTeams[index].id === sortedTeams[index + 1].id) {
            sortedTeams.splice(index + 1, 1);
          }
        }
        return sortedTeams;
      });

      _export('distinctPlayers', distinctPlayers = function distinctPlayers(players) {
        var sortedPlayers = players.sort(function (player1, player2) {
          return player1.name.localeCompare(player2.name);
        });
        for (var index = 0; index < sortedPlayers.length - 1; index++) {
          if (sortedPlayers[index].id === sortedPlayers[index + 1].id) {
            sortedPlayers.splice(index + 1, 1);
          }
        }
        return sortedPlayers;
      });

      _export('normalizePlayerResponse', normalizePlayerResponse = function normalizePlayerResponse(data) {
        var result = [];
        data.forEach(function (player) {
          if (player.jumps) {
            player.jumps.forEach(function (jump) {
              result.push({
                player: player.name,
                metric: 'Jump',
                value: normalizeJump(jump),
                timestamp: jump.time
              });
            });
          }
          if (player.energy) {
            player.energy.forEach(function (energy) {
              result.push({
                player: player.name,
                metric: 'Energy',
                value: energy.energy,
                timestamp: energy.time
              });
            });
          }
          if (player.explosions) {
            player.explosions.forEach(function (explosion) {
              result.push({
                player: player.name,
                metric: 'Explosion',
                value: explosion.index,
                timestamp: explosion.time
              });
            });
          }
          if (player.steps) {
            player.steps.forEach(function (step) {
              result.push({
                player: player.name,
                metric: 'Step',
                value: normalizeStep(step),
                timestamp: step.time
              });
            });
          }
          if (player.heartrates) {
            player.heartrates.forEach(function (heartrate) {
              result.push({
                player: player.name,
                metric: 'Heartrate',
                value: heartrate.heartrate,
                timestamp: heartrate.time
              });
            });
          }
          if (player.pressures) {
            player.pressures.forEach(function (pressure) {
              result.push({
                player: player.name,
                metric: 'Pressure',
                value: pressure.index,
                timestamp: pressure.time
              });
            });
          }
        });
        return result;
      });

      normalizeJump = function normalizeJump(jump) {
        return '\n  height:' + jump.height + '\n  duration:' + jump.duration + '\n  acceleration:' + jump.acceleration + '\n  index:' + jump.index + '\n  activity:' + jump.activity + '\n  samplingFrequency:' + jump.samplingFrequency + '\n  ';
      };

      normalizeStep = function normalizeStep(step) {
        return '\n  impact:' + step.impact + '\n  speed:' + step.speed + '\n  ';
      };

      _export('normalizeTeamResponse', normalizeTeamResponse = function normalizeTeamResponse(data) {
        var result = [];
        data.forEach(function (team) {
          if (team.noiseLevels) {
            team.noiseLevels.forEach(function (noiseLevel) {
              result.push({
                team: team.name,
                metric: 'Noise level',
                value: noiseLevel.decibel,
                timestamp: noiseLevel.time
              });
            });
          }
          if (team.fanAttitudes) {
            team.fanAttitudes.forEach(function (fanAttitude) {
              result.push({
                team: team.name,
                metric: 'Fan attitudes',
                value: normalizeFanAttitude(fanAttitude),
                timestamp: fanAttitude.time
              });
            });
          }
          if (team.audioClasses) {
            team.audioClasses.forEach(function (audioClass) {
              result.push({
                team: team.name,
                metric: 'Audio classes',
                value: normalizeAudioClass(audioClass),
                timestamp: audioClass.start
              });
            });
          }
        });
        return result;
      });

      normalizeFanAttitude = function normalizeFanAttitude(fanAttitude) {
        return '\n    attitude:' + fanAttitude.attitude + '\n    class:' + fanAttitude.class + '\n    noiseIndex:' + fanAttitude.noiseIndex + '\n    noiseLevel:' + fanAttitude.noiseLevel + '\n    teamName:' + fanAttitude.teamName + '\n  ';
      };

      normalizeAudioClass = function normalizeAudioClass(audioClass) {
        return '\n    class:' + audioClass.class + ',\n    stop:' + audioClass.stop + '\n  ';
      };

      _export('normalizePlayerAverageResponse', normalizePlayerAverageResponse = function normalizePlayerAverageResponse(data) {
        var result = [];
        data.forEach(function (player) {
          if (player.aggregatedEnergyAvg) {
            result.push({
              player: player.name,
              metric: 'Average aggregated energy',
              value: player.aggregatedEnergyAvg.energy
            });
          }
          if (player.explosionAvg) {
            result.push({
              player: player.name,
              metric: 'Average explosing',
              value: player.explosionAvg.index
            });
          }
          if (player.heartrateAvg) {
            result.push({
              player: player.name,
              metric: 'Average heartrate',
              value: player.heartrateAvg.heartrate
            });
          }
          if (player.pressureAvg) {
            result.push({
              player: player.name,
              metric: 'Average pressure',
              value: player.pressureAvg.index
            });
          }
          if (player.jumpAccelerationAvg || player.jumpDurationAvg || player.jumpHeightAvg || player.jumpIndexAvg) {
            result.push({
              player: player.name,
              metric: 'Average jump',
              value: normalizeAvgJump(player)
            });
          }
          if (player.stepImpactAvg || player.stepSpeedAvg) {
            result.push({
              player: player.name,
              metric: 'Average step',
              value: normalizeAvgStep(player)
            });
          }
        });
        return result;
      });

      normalizeAvgJump = function normalizeAvgJump(player) {
        var result = '';
        if (player.jumpAccelerationAvg) {
          result = result.concat('acceleration:' + player.jumpAccelerationAvg.acceleration + ' ');
        }
        if (player.jumpDurationAvg) {
          result = result.concat('duration:' + player.jumpDurationAvg.duration + ' ');
        }
        if (player.jumpHeightAvg) {
          result = result.concat('height:' + player.jumpHeightAvg.height + ' ');
        }
        if (player.jumpIndexAvg) {
          result = result.concat('index:' + player.jumpIndexAvg.index + ' ');
        }
        return result;
      };

      normalizeAvgStep = function normalizeAvgStep(player) {
        var result = '';
        if (player.stepImpactAvg) {
          result = result.concat('step:' + player.stepImpactAvg.impact + ' ');
        }
        if (player.stepSpeedAvg) {
          result = result.concat('speed:' + player.stepSpeedAvg.speed + ' ');
        }
        return result;
      };

      _export('normalizeTeamAverageResponse', normalizeTeamAverageResponse = function normalizeTeamAverageResponse(data) {
        var result = [];
        data.forEach(function (team) {
          if (team.aggregatedEnergyAvg) {
            result.push({
              team: team.name,
              metric: 'Average aggregated energy',
              value: team.aggregatedEnergyAvg.energy
            });
          }
          if (team.fanPeakAttitudeAvg) {
            result.push({
              team: team.name,
              metric: 'Average fan peak attitude',
              value: team.fanPeakAttitudeAvg.attitude
            });
          }
          if (team.fanAttitudeNoiseIndexAvg || team.fanAttitudeNoiseLevelAvg) {
            result.push({
              team: team.name,
              metric: 'Average fan attitude',
              value: normalizeAgvFanAttitude(team)
            });
          }
          if (team.jumpAccelerationAvg || team.jumpDurationAvg || team.jumpHeightAvg || team.jumpIndexAvg) {
            result.push({
              team: team.name,
              metric: 'Average jump',
              value: normalizeAvgJump(team)
            });
          }
        });
        return result;
      });

      normalizeAgvFanAttitude = function normalizeAgvFanAttitude(team) {
        var result = '';
        if (team.fanAttitudeNoiseIndexAvg) {
          result = result.concat('noiseIndex: ' + team.fanAttitudeNoiseIndexAvg.noiseIndex + ' ');
        }
        if (team.fanAttitudeNoiseLevelAvg) {
          result = result.concat('noiseLevel: ' + team.fanAttitudeNoiseLevelAvg.noiseLevel + ' ');
        }
        return result;
      };

      _export('normalizePlayerComparisonResponse', normalizePlayerComparisonResponse = function normalizePlayerComparisonResponse(data) {
        var result = [];
        data.forEach(function (player) {
          if (player.stepImpactSum) {
            result.push({
              player: player.name,
              metric: 'Step impact sum',
              value: player.stepImpactSum.impact
            });
          }
          if (player.explosionSum) {
            result.push({
              player: player.name,
              metric: 'Explosion sum',
              value: player.explosionSum.index
            });
          }
          if (player.aggregatedEnergySum) {
            result.push({
              player: player.name,
              metric: 'Aggregated energy sum',
              value: player.aggregatedEnergySum.energy
            });
          }
          if (player.jumpHeightSum) {
            result.push({
              player: player.name,
              metric: 'Jump height sum',
              value: player.jumpHeightSum.height
            });
          }
          if (player.jumpDurationSum) {
            result.push({
              player: player.name,
              metric: 'Jump duration sum',
              value: player.jumpDurationSum.duration
            });
          }
        });
        return result;
      });

      _export('normalizeTeamComparisonResponse', normalizeTeamComparisonResponse = function normalizeTeamComparisonResponse(data) {
        var result = [];
        data.forEach(function (team) {
          if (team.aggregatedEnergySum) {
            result.push({
              team: team.name,
              metric: 'Aggregated energy sum',
              value: team.aggregatedEnergySum.energy
            });
          }
          if (team.fanPeakAttitudeSum) {
            result.push({
              team: team.name,
              metric: 'Fan peak attitude sum',
              value: team.fanPeakAttitudeSum.attitude
            });
          }
          if (team.fanAttitudeNoiseIndexSum || team.fanAttitudeNoiseLevelSum) {
            result.push({
              team: team.name,
              metric: 'Fan attitude sum',
              value: normalizeSumFanAttitude(team)
            });
          }
          if (team.jumpAccelerationSum || team.jumpDurationSum || team.jumpHeightSum || team.jumpIndexSum) {
            result.push({
              team: team.name,
              metric: 'Jump sum',
              value: normalizeSumJump(team)
            });
          }
        });
        return result;
      });

      normalizeSumFanAttitude = function normalizeSumFanAttitude(team) {
        var result = '';
        if (team.fanAttitudeNoiseIndexSum) {
          result = result.concat('noiseIndex: ' + team.fanAttitudeNoiseIndexSum.noiseIndex + ' ');
        }
        if (team.fanAttitudeNoiseLevelSum) {
          result = result.concat('noiseLevel: ' + team.fanAttitudeNoiseLevelSum.noiseLevel + ' ');
        }
        return result;
      };

      normalizeSumJump = function normalizeSumJump(team) {
        var result = '';
        if (team.jumpAccelerationSum) {
          result = result.concat('acceleration:' + team.jumpAccelerationSum.acceleration + ' ');
        }
        if (team.jumpDurationSum) {
          result = result.concat('duration:' + team.jumpDurationSum.duration + ' ');
        }
        if (team.jumpHeightSum) {
          result = result.concat('height:' + team.jumpHeightSum.height + ' ');
        }
        if (team.jumpIndexSum) {
          result = result.concat('index:' + team.jumpIndexSum.index + ' ');
        }
        return result;
      };

      _export('calculateMinStartAndMaxEnd', calculateMinStartAndMaxEnd);

      _export('distinctTeams', distinctTeams);

      _export('distinctPlayers', distinctPlayers);

      _export('normalizePlayerResponse', normalizePlayerResponse);

      _export('normalizeTeamResponse', normalizeTeamResponse);

      _export('normalizePlayerAverageResponse', normalizePlayerAverageResponse);

      _export('normalizeTeamAverageResponse', normalizeTeamAverageResponse);

      _export('normalizePlayerComparisonResponse', normalizePlayerComparisonResponse);

      _export('normalizeTeamComparisonResponse', normalizeTeamComparisonResponse);
    }
  };
});
//# sourceMappingURL=utils.js.map
