'use strict';

System.register([], function (_export, _context) {
  "use strict";

  var _createClass, CsvService;

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  return {
    setters: [],
    execute: function () {
      _createClass = function () {
        function defineProperties(target, props) {
          for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];
            descriptor.enumerable = descriptor.enumerable || false;
            descriptor.configurable = true;
            if ("value" in descriptor) descriptor.writable = true;
            Object.defineProperty(target, descriptor.key, descriptor);
          }
        }

        return function (Constructor, protoProps, staticProps) {
          if (protoProps) defineProperties(Constructor.prototype, protoProps);
          if (staticProps) defineProperties(Constructor, staticProps);
          return Constructor;
        };
      }();

      _export('CsvService', CsvService = function () {
        function CsvService(csvDelimiter) {
          _classCallCheck(this, CsvService);

          this.csvDelimiter = csvDelimiter;
        }

        _createClass(CsvService, [{
          key: 'exportTableToCSV',
          value: function exportTableToCSV(filename) {
            var csv = [];
            var rows = document.querySelectorAll('table tr');

            for (var index = 0; index < rows.length; index++) {
              var row = [];
              var cols = rows[index].querySelectorAll('td, th');

              for (var jIndex = 0; jIndex < cols.length; jIndex++) {
                row.push(cols[jIndex].innerText);
              }

              csv.push(row.join(this.csvDelimiter));
            }

            // Download CSV file
            this.downloadCSV(csv.join('\n'), filename);
          }
        }, {
          key: 'downloadCSV',
          value: function downloadCSV(csv, filename) {
            // CSV file
            var csvFile = new Blob([csv], { type: 'text/csv' });

            // Download link
            var downloadLink = document.createElement('a');

            // File name
            downloadLink.download = filename;

            // Create a link to the file
            downloadLink.href = window.URL.createObjectURL(csvFile);

            // Hide download link
            downloadLink.style.display = 'none';

            // Add the link to DOM
            document.body.appendChild(downloadLink);

            // Click download link
            downloadLink.click();
          }
        }]);

        return CsvService;
      }());

      _export('CsvService', CsvService);
    }
  };
});
//# sourceMappingURL=csv_service.js.map
