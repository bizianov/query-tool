/**
 * Created by viacheslav on 11/30/17.
 */
const getEventsListWithPlayers =
    `query getEventsListWithPlayers {
      sports {
        games {
          id,  
          shortName,
          duration {
            start,
            end
          },
          homeTeam {
            id,
            name,
            players {
              id,
              name
            }
          },
          awayTeam {
            id,
            name,
            players {
              id,
              name
            }
          }
        }
      }
    }`;

const getAllSensorDataByPlayerNameInTimeRange =
    `query Q1_getAllSensorDataByPlayerNameInTimeRange(
          $playerName: String,
          $resultAmount: Int,
          $startActionTime: Date!,
          $endActionTime: Date!,
          $includeHeartrates: Boolean!,
          $includePressures: Boolean!,
          $includeSteps: Boolean!,
          $includeExplosions: Boolean!,
          $includeEnergy: Boolean!,
          $includeJumps: Boolean!) {
      sports {
        players(name: $playerName) {
          ...playerFields
          heartrates(first: $resultAmount, orderBy: time,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeHeartrates) {
            ...heartrateFields
          }
          pressures(first: $resultAmount, orderBy: time,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includePressures) {
            ...pressureFields
          }
          steps(first: $resultAmount, orderBy: time,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeSteps) {
            ...stepFields
          }
          explosions(first: $resultAmount, orderBy: time,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeExplosions) {
            ...explosionFields
          }
          energy(first: $resultAmount, orderBy: time,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeEnergy) {
            ...energyFields
          }
          jumps(first: $resultAmount, orderBy: start,
            during: {start: $startActionTime, end: $endActionTime}) @include(if: $includeJumps) {
            ...jumpFields
          }
        }
      }
    }

    fragment heartrateFields on sportsr_HeartrateWithPlayerStatusAttribute {
      time
      heartrate
    }
    
    fragment pressureFields on sportsr_PressureIndexAttribute {
      time
      index
    }
    
    fragment stepFields on sportsr_StepAttribute {
      impact
      speed
      time
    }
    
    fragment explosionFields on sportsr_ExplosionAttribute {
      index
      time
    }
    
    fragment jumpFields on sportsr_JumpAttribute {
      time: start
      end
      activity
      samplingFrequency
      index
      acceleration
      height
      duration
    }
    
    fragment energyFields on sportsr_EnergyAttribute {
      energy
      time
    }
    
    fragment playerFields on sports_BasketballPlayer_Type {
      name
      label
    }`;

const getAllSensorDataByTeamNameInTimeRange =
    `query Q2_getAllSensorDataByTeamNameInTimeRange(
          $teamName: String,
          $resultAmount: Int,
          $startActionTime: Date!,
          $endActionTime: Date!,
          $includeNoiseLevels: Boolean!,
          $includeAudioClasses: Boolean!,
          $includeFanAttitudes: Boolean!) {
      sports {
        teams(name: $teamName) {
          ...teamFields
          noiseLevels(first: $resultAmount, orderBy: time,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeNoiseLevels) {
            ...noiseLevelsFields
          }
          fanAttitudes(first: $resultAmount, orderBy: time,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeFanAttitudes) {
            ...fanAttitudesFields
          }
          audioClasses(first: $resultAmount, orderBy: start,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeAudioClasses) {
            ...audioClassesFields
          }
        }
      }
    }
    
    fragment noiseLevelsFields on sportsr_TeamNoiseLevelAttribute {
      time
      decibel
    }
    
    fragment teamFields on sports_Team_Type {
      shortName
      name
      label
    }
    
    fragment fanAttitudesFields on sportsr_TeamAttitudeAttribute {
      noiseLevel
      noiseIndex
      class
      teamName
      attitude
      time
    }
    
    fragment audioClassesFields on sportsr_TeamAudioAttribute {
      stop
      start
      class
    }`;

const getPlayerAverageDateInTimeRange =
    `query Q3_playerAverageDataInTimeRange(
          $playerName: String,
          $startActionTime: Date!,
          $endActionTime: Date!,
          $includeHeartrates: Boolean!,
          $includePressures: Boolean!,
          $includeSteps: Boolean!,
          $includeExplosions: Boolean!,
          $includeEnergy: Boolean!,
          $includeJumps: Boolean!) {
          sports {
        players(name: $playerName) {
          ...playerFields
          heartrateAvg: heartrate(avg: heartrate,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeHeartrates) {
            heartrate
          }
          pressureAvg: pressure(avg: index,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includePressures) {
            index
          }
          stepSpeedAvg: step(avg: speed,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeSteps) {
            speed
          }
          stepImpactAvg: step(avg: ,impact
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeSteps) {
            impact
          }
    
          explosionAvg: explosion(avg: index,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeExplosions) {
            index
          }
    
          aggregatedEnergyAvg: aggregatedEnergy(avg: energy
          during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeEnergy){
            energy
          }
    
          jumpIndexAvg: jump(avg: index,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            index
          }
    
          jumpAccelerationAvg: jump(avg: acceleration,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            acceleration
          }
    
          jumpHeightAvg: jump(avg: height,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            height
          }
    
          jumpDurationAvg: jump(avg: duration,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            duration
          }
        }
      }
    }
    
    fragment playerFields on sports_BasketballPlayer_Type {
      name
      label
    }`;

const getTeamAverageDateInTimeRange =
    `query Q4_teamAverageDataInTimeRange(
          $teamName: String,
          $startActionTime: Date!,
          $endActionTime: Date!,
          $includeFanAttitudes: Boolean!,
          $includeFanPeakAttitudes: Boolean!,
          $includeJumps: Boolean!,
          $includeEnergy: Boolean!) {
      sports {
        teams(name: $teamName){
          ...teamFields
    
          fanPeakAttitudeAvg: fanPeakAttitude(avg: attitude,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeFanPeakAttitudes) {
            attitude
          }
    
          fanAttitudeNoiseLevelAvg: fanAttitude(avg: noiseLevel,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeFanAttitudes) {
            noiseLevel
          }
    
          fanAttitudeNoiseIndexAvg: fanAttitude(avg: noiseIndex,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeFanAttitudes) {
            noiseIndex
          }
    
          jumpIndexAvg: jump(avg: index,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            index
          }
    
          jumpAccelerationAvg: jump(avg: acceleration,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            acceleration
          }
    
          jumpHeightAvg: jump(avg: height,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            height
          }
    
          jumpDurationAvg: jump(avg: duration,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            duration
          }
    
          aggregatedEnergyAvg: aggregatedEnergy(avg: energy,
          during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeEnergy) {
            energy
          }
        }
      }
    }
    
    fragment teamFields on sports_Team_Type {
      shortName
      name
      label
    }
    `;

const getPlayerSumDataInTimeRange =
    `query Q7_playerSumDataInTimeRange(
          $playerName: String,
          $startActionTime: Date!,
          $endActionTime: Date!,
          $includeSteps: Boolean!,
          $includeExplosions: Boolean!,
          $includeEnergy: Boolean!,
          $includeJumps: Boolean!) {
      sports {
        players(name: $playerName) {
          ...playerFields
    
          stepImpactSum: step(sum: ,impact
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeSteps) {
            impact
          }
    
          explosionSum: explosion(sum: index,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeExplosions) {
            index
          }
    
          aggregatedEnergySum: aggregatedEnergy(sum: energy
          during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeEnergy){
            energy
          }
    
          jumpHeightSum: jump(sum: height,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            height
          }
    
          jumpDurationSum: jump(sum: duration,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            duration
          }
        }
      }
    }
    
    fragment playerFields on sports_BasketballPlayer_Type {
      name
      label
    }
    `;

const getTeamSumDataInTimeRange =
    `query Q8_teamSumDataInTimeRange(
          $teamName: String,
          $startActionTime: Date!,
          $endActionTime: Date!,
          $includeFanAttitudes: Boolean!,
          $includeFanPeakAttitudes: Boolean!,
          $includeJumps: Boolean!,
          $includeEnergy: Boolean!) {
      sports {
        teams(name: $teamName){
          ...teamFields
    
          fanPeakAttitudeSum: fanPeakAttitude(sum: attitude,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeFanPeakAttitudes) {
            attitude
          }
    
          fanAttitudeNoiseLevelSum: fanAttitude(sum: noiseLevel,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeFanAttitudes) {
            noiseLevel
          }
    
          fanAttitudeNoiseIndexSum: fanAttitude(sum: noiseIndex,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeFanAttitudes) {
            noiseIndex
          }
    
          jumpIndexSum: jump(sum: index,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            index
          }
    
          jumpAccelerationSum: jump(sum: acceleration,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            acceleration
          }
    
          jumpHeightSum: jump(sum: height,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            height
          }
    
          jumpDurationSum: jump(sum: duration,
            during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeJumps) {
            duration
          }
    
          aggregatedEnergySum: aggregatedEnergy(sum: energy,
          during: {start: $startActionTime, end: $endActionTime})
          @include(if: $includeEnergy) {
            energy
          }
        }
      }
    }
    
    fragment teamFields on sports_Team_Type {
      shortName
      name
      label
    }
    `;

const singlePlayerComment = 'Query is used to get all the sensor data for one specific player.\
 If multiple players are chosen all except the first one will be ignored';

const singleTeamComment = 'Query is used to get all the sensor data for one specific team.\
 If multiple teams are chosen all except the first one will be ignored';

const singlePlayerAvgComment = 'Query is used to get average sensor data for one specific player.\
 If multiple players are chosen all except the first one will be ignored';

const singleTeamAvgComment = 'Query is used to get average sensor data for one specific team.\
 If multiple teams are chosen all except the first one will be ignored';

const twoPlayersComment = 'Query is used to compare the sensor data of 2 players.\
 If more than 2 players are chosen all except the first two players will be ignored.\
 Fields available for comparing: Steps, Energy, Jumps, Explosions';

const twoTeamsComment = 'Query is used to compare the sensor data of 2 teams.\
 If more than 2 teams are chosen all except the first two teams will be ignored';

export {
    getEventsListWithPlayers,
    getAllSensorDataByPlayerNameInTimeRange,
    getAllSensorDataByTeamNameInTimeRange,
    getPlayerAverageDateInTimeRange,
    getTeamAverageDateInTimeRange,
    getPlayerSumDataInTimeRange,
    getTeamSumDataInTimeRange,
    singlePlayerAvgComment,
    singleTeamAvgComment,
    singlePlayerComment,
    singleTeamComment,
    twoPlayersComment,
    twoTeamsComment};
