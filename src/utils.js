/**
 * If multiple events selected we should render data in the time range [minStart, maxEnd]
 */
const calculateMinStartAndMaxEnd = (events) => {
  const minStart = events.map((event) => {
    return event.duration.start;
  }).sort().shift();
  const maxEnd = events.map((event) => {
    return event.duration.end;
  }).sort().pop();
  return {
    startTime: minStart,
    endTime: maxEnd
  };
};

/**
 * Find only distinct teams
 * Algorithm: sort array by team.name; if two adjacent elements have the same id - delete one of them
 */
const distinctTeams = (teams) => {
  const sortedTeams = teams.sort((team1, team2) => team1.name.localeCompare(team2.name));
  for (let index = 0; index < sortedTeams.length - 1; index++) {
    if (sortedTeams[index].id === sortedTeams[index + 1].id) {
      sortedTeams.splice(index + 1, 1);
    }
  }
  return sortedTeams;
};

/**
 * Find only distinct players
 * Algorithm: sort array by player.name; if two adjacent elements have the same id - delete one of them
 */
const distinctPlayers = (players) => {
  const sortedPlayers = players.sort((player1, player2) => player1.name.localeCompare(player2.name));
  for (let index = 0; index < sortedPlayers.length - 1; index++) {
    if (sortedPlayers[index].id === sortedPlayers[index + 1].id) {
      sortedPlayers.splice(index + 1, 1);
    }
  }
  return sortedPlayers;
};

/**
 * Prepare player-related query response from GraphQL to be displayed in the result table
 * Columns: [playerName, metric, value, timestamp]
 */
const normalizePlayerResponse = (data) => {
  const result = [];
  data.forEach((player) => {
    if (player.jumps) {
      player.jumps.forEach((jump) => {
        result.push({
          player: player.name,
          metric: 'Jump',
          value: normalizeJump(jump),
          timestamp: jump.time
        });
      });
    }
    if (player.energy) {
      player.energy.forEach((energy) => {
        result.push({
          player: player.name,
          metric: 'Energy',
          value: energy.energy,
          timestamp: energy.time
        });
      });
    }
    if (player.explosions) {
      player.explosions.forEach((explosion) => {
        result.push({
          player: player.name,
          metric: 'Explosion',
          value: explosion.index,
          timestamp: explosion.time
        });
      });
    }
    if (player.steps) {
      player.steps.forEach((step) => {
        result.push({
          player: player.name,
          metric: 'Step',
          value: normalizeStep(step),
          timestamp: step.time
        });
      });
    }
    if (player.heartrates) {
      player.heartrates.forEach((heartrate) => {
        result.push({
          player: player.name,
          metric: 'Heartrate',
          value: heartrate.heartrate,
          timestamp: heartrate.time
        });
      });
    }
    if (player.pressures) {
      player.pressures.forEach((pressure) => {
        result.push({
          player: player.name,
          metric: 'Pressure',
          value: pressure.index,
          timestamp: pressure.time
        });
      });
    }
  });
  return result;
};

const normalizeJump = (jump) => {
  return `
  height:${jump.height}
  duration:${jump.duration}
  acceleration:${jump.acceleration}
  index:${jump.index}
  activity:${jump.activity}
  samplingFrequency:${jump.samplingFrequency}
  `;
};

const normalizeStep = (step) => {
  return `
  impact:${step.impact}
  speed:${step.speed}
  `;
};

/**
 * Prepare team-related query response from GraphQL to be displayed in the result table
 * Columns: [teamName, metric, value, timestamp]
 */
const normalizeTeamResponse = (data) => {
  const result = [];
  data.forEach((team) => {
    if (team.noiseLevels) {
      team.noiseLevels.forEach((noiseLevel) => {
        result.push({
          team: team.name,
          metric: 'Noise level',
          value: noiseLevel.decibel,
          timestamp: noiseLevel.time
        });
      });
    }
    if (team.fanAttitudes) {
      team.fanAttitudes.forEach((fanAttitude) => {
        result.push({
          team: team.name,
          metric: 'Fan attitudes',
          value: normalizeFanAttitude(fanAttitude),
          timestamp: fanAttitude.time
        });
      });
    }
    if (team.audioClasses) {
      team.audioClasses.forEach((audioClass) => {
        result.push({
          team: team.name,
          metric: 'Audio classes',
          value: normalizeAudioClass(audioClass),
          timestamp: audioClass.start
        });
      });
    }
  });
  return result;
};

const normalizeFanAttitude = (fanAttitude) => {
  return `
    attitude:${fanAttitude.attitude}
    class:${fanAttitude.class}
    noiseIndex:${fanAttitude.noiseIndex}
    noiseLevel:${fanAttitude.noiseLevel}
    teamName:${fanAttitude.teamName}
  `;
};

const normalizeAudioClass = (audioClass) => {
  return `
    class:${audioClass.class},
    stop:${audioClass.stop}
  `;
};
const normalizePlayerAverageResponse = (data) => {
  const result = [];
  data.forEach((player) => {
    if (player.aggregatedEnergyAvg) {
      result.push({
        player: player.name,
        metric: 'Average aggregated energy',
        value: player.aggregatedEnergyAvg.energy
      });
    }
    if (player.explosionAvg) {
      result.push({
        player: player.name,
        metric: 'Average explosing',
        value: player.explosionAvg.index
      });
    }
    if (player.heartrateAvg) {
      result.push({
        player: player.name,
        metric: 'Average heartrate',
        value: player.heartrateAvg.heartrate
      });
    }
    if (player.pressureAvg) {
      result.push({
        player: player.name,
        metric: 'Average pressure',
        value: player.pressureAvg.index
      });
    }
    if (player.jumpAccelerationAvg || player.jumpDurationAvg || player.jumpHeightAvg || player.jumpIndexAvg) {
      result.push({
        player: player.name,
        metric: 'Average jump',
        value: normalizeAvgJump(player)
      });
    }
    if (player.stepImpactAvg || player.stepSpeedAvg) {
      result.push({
        player: player.name,
        metric: 'Average step',
        value: normalizeAvgStep(player)
      });
    }
  });
  return result;
};

const normalizeAvgJump = (player) => {
  let result = '';
  if (player.jumpAccelerationAvg) {
    result = result.concat(`acceleration:${player.jumpAccelerationAvg.acceleration} `);
  }
  if (player.jumpDurationAvg) {
    result = result.concat(`duration:${player.jumpDurationAvg.duration} `);
  }
  if (player.jumpHeightAvg) {
    result = result.concat(`height:${player.jumpHeightAvg.height} `);
  }
  if (player.jumpIndexAvg) {
    result = result.concat(`index:${player.jumpIndexAvg.index} `);
  }
  return result;
};

const normalizeAvgStep = (player) => {
  let result = '';
  if (player.stepImpactAvg) {
    result = result.concat(`step:${player.stepImpactAvg.impact} `);
  }
  if (player.stepSpeedAvg) {
    result = result.concat(`speed:${player.stepSpeedAvg.speed} `);
  }
  return result;
};

const normalizeTeamAverageResponse = (data) => {
  const result = [];
  data.forEach((team) => {
    if (team.aggregatedEnergyAvg) {
      result.push({
        team: team.name,
        metric: 'Average aggregated energy',
        value: team.aggregatedEnergyAvg.energy
      });
    }
    if (team.fanPeakAttitudeAvg) {
      result.push({
        team: team.name,
        metric: 'Average fan peak attitude',
        value: team.fanPeakAttitudeAvg.attitude
      });
    }
    if (team.fanAttitudeNoiseIndexAvg || team.fanAttitudeNoiseLevelAvg) {
      result.push({
        team: team.name,
        metric: 'Average fan attitude',
        value: normalizeAgvFanAttitude(team)
      });
    }
    if (team.jumpAccelerationAvg || team.jumpDurationAvg || team.jumpHeightAvg || team.jumpIndexAvg) {
      result.push({
        team: team.name,
        metric: 'Average jump',
        value: normalizeAvgJump(team)
      });
    }
  });
  return result;
};

const normalizeAgvFanAttitude = (team) => {
  let result = '';
  if (team.fanAttitudeNoiseIndexAvg) {
    result = result.concat(`noiseIndex: ${team.fanAttitudeNoiseIndexAvg.noiseIndex} `);
  }
  if (team.fanAttitudeNoiseLevelAvg) {
    result = result.concat(`noiseLevel: ${team.fanAttitudeNoiseLevelAvg.noiseLevel} `);
  }
  return result;
};

const normalizePlayerComparisonResponse = (data) => {
  const result = [];
  data.forEach((player) => {
    if (player.stepImpactSum) {
      result.push({
        player: player.name,
        metric: 'Step impact sum',
        value: player.stepImpactSum.impact
      });
    }
    if (player.explosionSum) {
      result.push({
        player: player.name,
        metric: 'Explosion sum',
        value: player.explosionSum.index
      });
    }
    if (player.aggregatedEnergySum) {
      result.push({
        player: player.name,
        metric: 'Aggregated energy sum',
        value: player.aggregatedEnergySum.energy
      });
    }
    if (player.jumpHeightSum) {
      result.push({
        player: player.name,
        metric: 'Jump height sum',
        value: player.jumpHeightSum.height
      });
    }
    if (player.jumpDurationSum) {
      result.push({
        player: player.name,
        metric: 'Jump duration sum',
        value: player.jumpDurationSum.duration
      });
    }
  });
  return result;
};

const normalizeTeamComparisonResponse = (data) => {
  const result = [];
  data.forEach((team) => {
    if (team.aggregatedEnergySum) {
      result.push({
        team: team.name,
        metric: 'Aggregated energy sum',
        value: team.aggregatedEnergySum.energy
      });
    }
    if (team.fanPeakAttitudeSum) {
      result.push({
        team: team.name,
        metric: 'Fan peak attitude sum',
        value: team.fanPeakAttitudeSum.attitude
      });
    }
    if (team.fanAttitudeNoiseIndexSum || team.fanAttitudeNoiseLevelSum) {
      result.push({
        team: team.name,
        metric: 'Fan attitude sum',
        value: normalizeSumFanAttitude(team)
      });
    }
    if (team.jumpAccelerationSum || team.jumpDurationSum || team.jumpHeightSum || team.jumpIndexSum) {
      result.push({
        team: team.name,
        metric: 'Jump sum',
        value: normalizeSumJump(team)
      });
    }
  });
  return result;
};

const normalizeSumFanAttitude = (team) => {
  let result = '';
  if (team.fanAttitudeNoiseIndexSum) {
    result = result.concat(`noiseIndex: ${team.fanAttitudeNoiseIndexSum.noiseIndex} `);
  }
  if (team.fanAttitudeNoiseLevelSum) {
    result = result.concat(`noiseLevel: ${team.fanAttitudeNoiseLevelSum.noiseLevel} `);
  }
  return result;
};

const normalizeSumJump = (team) => {
  let result = '';
  if (team.jumpAccelerationSum) {
    result = result.concat(`acceleration:${team.jumpAccelerationSum.acceleration} `);
  }
  if (team.jumpDurationSum) {
    result = result.concat(`duration:${team.jumpDurationSum.duration} `);
  }
  if (team.jumpHeightSum) {
    result = result.concat(`height:${team.jumpHeightSum.height} `);
  }
  if (team.jumpIndexSum) {
    result = result.concat(`index:${team.jumpIndexSum.index} `);
  }
  return result;
};

export {calculateMinStartAndMaxEnd, distinctTeams, distinctPlayers, normalizePlayerResponse, normalizeTeamResponse,
    normalizePlayerAverageResponse, normalizeTeamAverageResponse, normalizePlayerComparisonResponse,
    normalizeTeamComparisonResponse};
