import {PanelCtrl} from 'app/plugins/sdk';
import {QueryService} from './query_service';
import {CsvService} from './csv_service';
// import {Spinner} from 'spin.js';
import _ from 'lodash';
import './css/query-panel.css!';
import {calculateMinStartAndMaxEnd, distinctTeams, distinctPlayers, normalizePlayerResponse, normalizeTeamResponse,
    normalizePlayerAverageResponse, normalizeTeamAverageResponse, normalizePlayerComparisonResponse,
    normalizeTeamComparisonResponse} from './utils';
import {singlePlayerComment, singleTeamComment, singlePlayerAvgComment, singleTeamAvgComment,
    twoPlayersComment, twoTeamsComment} from './queries';

const panelDefaults = {
  projects: [
    {name: 'Euroleague', code: 0}
  ],
  currentProject: {},
  events: [],
  currentEvents: [],
  queries: [
    {name: 'Sensor data by player and type', code: 0, comment: singlePlayerComment},
    {name: 'Sensor data by team and type', code: 1, comment: singleTeamComment},
    {name: 'Average sensor data by player', code: 2, comment: singlePlayerAvgComment},
    {name: 'Average sensor data by team', code: 3, comment: singleTeamAvgComment},
    {name: 'Compare 2 players', code: 6, comment: twoPlayersComment},
    {name: 'Compare 2 teams', code: 7, comment: twoTeamsComment}
  ],
  currentQuery: {},
  playerQueryCodes: [0, 2, 6],
  teamQueryCodes: [1, 3, 7],
  teamAvgQueryCodes: [3, 7],
  comparisonQueryCodes: [6, 7],
  playerMetrics: [
    {name: 'Jumps', code: 0},
    {name: 'Heartrates', code: 1},
    {name: 'Steps', code: 2},
    {name: 'Energy', code: 3},
    {name: 'Pressures', code: 4},
    {name: 'Explosions', code: 5},
  ],
  currentPlayerMetrics: [],
  players: [],
  currentPlayers: [],
  teamMetrics: [
    {name: 'Noise level', code: 0},
    {name: 'Fan attitudes', code: 1},
    {name: 'Audio classes', code: 2}
  ],
  teamAvgMetrics: [
    {name: 'Jumps', code: 3},
    {name: 'Energy', code: 4},
    {name: 'Fan attitudes', code: 5},
    {name: 'Fan peak attitudes', code: 6},
  ],
  currentTeamMetrics: [],
  teams: [],
  currentTeams: [],
  timeRange: {},
  customTimeRange: {},
  resultPrecision: 10,
  showPlayerTable: false,
  showTeamTable: false,
  noDataFoundAlert: false,
  notEnoughFilters: false,
  data: [],
  loadingData: false,
  csvDelimiters: ['Comma ,', 'Pipe |'],
  csvDelimiter: ',',
  // TODO: should be empty by default
  url: 'heedprod.heedplatform.iotarndgermany.de:38443/graphql',
  username: 'guest',
  password: 'cv%6xYFPaev!PLU5'
};

export class QueryCtrl extends PanelCtrl {
  constructor($scope, $injector, $http) {
    super($scope, $injector, $http);
    _.defaultsDeep(this.panel, panelDefaults);
    this.events.on('panel-initialized', this.render.bind(this));
    this.events.on('init-edit-mode', this.onInitEditMode.bind(this));
    this.$http = $http;
    // TODO: remove after clear username/password
    this.queryService = new QueryService(this.$http, this.panel.url, this.panel.username, this.panel.password);
  }

  onInitEditMode() {
    this.addEditorTab('Options', 'public/plugins/grafana-query-panel/editor.html', 2);
  }

  saveCredentials() {
    this.queryService = new QueryService(this.$http, this.panel.username, this.panel.password);
  }

  projectChanged() {
    const panel = this.panel;
    console.log('Current project: ' + panel.currentProject.name);
    const variables = {};
    this.queryService.getEventsListWithPlayers(variables)
          .then((response) => {
            panel.events = response.data.data.sports.games.map((event) => {
              return {
                id: event.id,
                name: event.shortName,
                duration: event.duration,
                homeTeam: event.homeTeam,
                awayTeam: event.awayTeam
              };
            });
          });
  }

  /**
   * After each event change the following calculations are done:
   * 1. calculateMinStartAndMaxEnd - time range between min start and max end among all the selected events
   * 2. distinctTeams - each event has 2 teams, hence if two events contains the same team it will be showed once only
   * 3. distinctPlayers - the same as previous case but related to players
   */
  eventChanged() {
    this.resetFilters();
    const events = this.panel.currentEvents;
    console.log('Current events: ', events);
    this.panel.timeRange = calculateMinStartAndMaxEnd(events);
    this.panel.customTimeRange = {
      startTime: this.panel.timeRange.startTime,
      endTime: this.panel.timeRange.endTime
    };
    const teams = [];
    events.forEach((event) => {
      teams.push(event.homeTeam, event.awayTeam);
    });
    this.panel.teams = distinctTeams(teams);
    let players = [];
    events.forEach((event) => {
      players = players.concat(event.homeTeam.players);
      players = players.concat(event.awayTeam.players);
    });
    this.panel.players = distinctPlayers(players);
  }

  queryChanged() {
    console.log('Current query: ', this.panel.currentQuery.name);
    this.resetFilters();
  }

  showTableContent() {
    this.panel.loadingData = true;
    this.panel.noDataFoundAlert = false;
    this.panel.notEnoughFilters = false;
    this.panel.showPlayerTable = false;
    this.panel.showTeamTable = false;

    let variables = {};
    let playersProcessed = 0;
    let teamsProcessed = 0;
    this.panel.data = [];

    if (this.panel.currentPlayerMetrics.length * this.panel.currentPlayers.length > 0) {
      const currentPlayerMetricsNames = this.panel.currentPlayerMetrics.map((metric) => metric.name);
      const playersToBeProcessed = this.slicePlayersToBeProcessed();
      playersToBeProcessed.forEach((player) => {
        variables = this.preparePlayerVariables(player, currentPlayerMetricsNames);
        this.queryService.executeQuery(this.panel.currentQuery, variables)
              .then((response) => {
                this.panel.data = this.panel.data.concat(this.normalizeResponse(response.data.data.sports.players));
                playersProcessed++;
                if (playersToBeProcessed.length === playersProcessed) {
                  this.panel.data = this.prepareComparePlayersView();
                  this.panel.showPlayerTable = true;
                  this.panel.noDataFoundAlert = true;
                  this.panel.loadingData = false;
                }
              });
      });
    } else if (this.panel.currentTeamMetrics.length * this.panel.currentTeams.length > 0) {
      const currentTeamMetricsNames = this.panel.currentTeamMetrics.map((metric) => metric.name);
      const teamsToBeProcessed = this.sliceTeamsToBeProcessed();
      teamsToBeProcessed.forEach((team) => {
        variables = this.prepareTeamVariables(team, currentTeamMetricsNames);
        this.queryService.executeQuery(this.panel.currentQuery, variables)
              .then((response) => {
                this.panel.data = this.panel.data.concat(this.normalizeResponse(response.data.data.sports.teams));
                teamsProcessed++;
                if (teamsToBeProcessed.length === teamsProcessed) {
                  console.log(this.panel.data);
                  this.panel.data = this.prepareCompareTeamsView();
                  this.panel.showTeamTable = true;
                  this.panel.noDataFoundAlert = true;
                  this.panel.loadingData = false;
                }
              });
      });
    } else {
      this.panel.notEnoughFilters = true;
      this.panel.loadingData = false;
    }
  }

  /**
   * Only query 6 and 7 accept multiple players|teams, hence slice (0, 2),
   * the rest should be limited to the first element only
   */
  slicePlayersToBeProcessed() {
    let playersToBeProcessed = [];
    if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
      playersToBeProcessed = this.panel.currentPlayers.slice(0, 2);
    } else {
      playersToBeProcessed = this.panel.currentPlayers.slice(0, 1);
    }
    return playersToBeProcessed;
  }

  /**
   * See slicePlayersToBeProcessed()
   */
  sliceTeamsToBeProcessed() {
    let teamsToBeProcessed = [];
    if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
      teamsToBeProcessed = this.panel.currentTeams.slice(0, 2);
    } else {
      teamsToBeProcessed = this.panel.currentTeams.slice(0, 1);
    }
    return teamsToBeProcessed;
  }

  prepareComparePlayersView() {
    let compareViewResult = [];
    if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
      const data = this.panel.data;
      for (let index = 0; index < data.length / 2; index++) {
        compareViewResult.push({
          player: `${data[index].player} / ${data[index + data.length / 2].player}`,
          metric: data[index].metric,
          value: `${data[index].value} / ${data[index + data.length / 2].value}`
        });
      }
    } else {
      compareViewResult = this.panel.data;
    }
    return compareViewResult;
  }

  prepareCompareTeamsView() {
    let compareViewResult = [];
    if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
      const data = this.panel.data;
      for (let index = 0; index < data.length / 2; index++) {
        compareViewResult.push({
          team: `${data[index].team} / ${data[index + data.length / 2].team}`,
          metric: data[index].metric,
          value: `${data[index].value} / ${data[index + data.length / 2].value}`
        });
      }
    } else {
      compareViewResult = this.panel.data;
    }
    return compareViewResult;
  }


  normalizeResponse(response) {
    switch (this.panel.currentQuery.code) {
      case 0:
        return normalizePlayerResponse(response);
      case 1:
        return normalizeTeamResponse(response);
      case 2:
        return normalizePlayerAverageResponse(response);
      case 3:
        return normalizeTeamAverageResponse(response);
      case 6:
        return normalizePlayerComparisonResponse(response);
      case 7:
        return normalizeTeamComparisonResponse(response);
      default:
        return [];
    }
  }


  /**
   * Prepare variables object that will be passed along with player-related query to QueryService.
   * It gives a possibility to use variables in the query, instead of hardcoding values.
   */
  preparePlayerVariables(player, playerMetricsNames) {
    return {
      playerName: player.name,
      resultAmount: this.panel.resultPrecision,
      startActionTime: this.panel.customTimeRange.startTime,
      endActionTime: this.panel.customTimeRange.endTime,
      includeJumps: playerMetricsNames.indexOf('Jumps') > -1,
      includeSteps: playerMetricsNames.indexOf('Steps') > -1,
      includeEnergy: playerMetricsNames.indexOf('Energy') > -1,
      includePressures: playerMetricsNames.indexOf('Pressures') > -1,
      includeHeartrates: playerMetricsNames.indexOf('Heartrates') > -1,
      includeExplosions: playerMetricsNames.indexOf('Explosions') > -1
    };
  }

  /**
   * Prepare variables object that will be passed along with team-related query to QueryService.
   * It gives a possibility to use variables in the query, instead of hardcoding values.
   */
  prepareTeamVariables(team, currentTeamMetricsNames) {
    let variables = {};
    // TODO: consider if it's possible not to be bind on metrics name (metric.code is better)
    if (this.panel.teamMetrics.map((metric) => metric.name).indexOf(currentTeamMetricsNames[0]) > -1) {
      variables = {
        teamName: team.name,
        resultAmount: this.panel.resultPrecision,
        startActionTime: this.panel.customTimeRange.startTime,
        endActionTime: this.panel.customTimeRange.endTime,
        includeNoiseLevels: currentTeamMetricsNames.indexOf('Noise level') > -1,
        includeFanAttitudes: currentTeamMetricsNames.indexOf('Fan attitudes') > -1,
        includeAudioClasses: currentTeamMetricsNames.indexOf('Audio classes') > -1
      };
    } else {
      variables = {
        teamName: team.name,
        resultAmount: this.panel.resultPrecision,
        startActionTime: this.panel.customTimeRange.startTime,
        endActionTime: this.panel.customTimeRange.endTime,
        includeJumps: currentTeamMetricsNames.indexOf('Jumps') > -1,
        includeEnergy: currentTeamMetricsNames.indexOf('Energy') > -1,
        includeFanAttitudes: currentTeamMetricsNames.indexOf('Fan attitudes') > -1,
        includeFanPeakAttitudes: currentTeamMetricsNames.indexOf('Fan peak attitudes') > -1,
      };
    }
    return variables;
  }

  /**
   * Switching between different queries result table, current metrics/players/teams should be cleared out.
   */
  resetFilters() {
    this.panel.currentTeamMetrics = [];
    this.panel.currentTeams = [];
    this.panel.currentPlayerMetrics = [];
    this.panel.currentPlayers = [];
    this.panel.data = [];
    this.panel.noDataFoundAlert = false;
    this.panel.notEnoughFilters = false;
  }

  /**
   * Default timeRange is minStartTime and maxEndTime across all the selected events.
   * These fields are editable and user can amend them due to desired time range.
   * This method is used to return timeRange to the default value.
   */
  resetTimeToDefault() {
    this.panel.customTimeRange = {
      startTime: this.panel.timeRange.startTime,
      endTime: this.panel.timeRange.endTime
    };
  }

  playerMetricsChanged() {
    console.log('Current actions: ', this.panel.currentPlayerMetrics);
  }

  teamMetricsChanges() {
    console.log('Current team metrics: ', this.panel.currentTeamMetrics);
  }

  playerChanged() {
    console.log('Current players: ', this.panel.currentPlayers);
  }

  teamChanged() {
    console.log('Current teams: ', this.panel.currentTeams);
  }

  copyTableContentAsTable() {
    const csvService = new CsvService(this.panel.csvDelimiter);
    const fileName = this.generateCsvFileName();
    csvService.exportTableToCSV(`${fileName}.csv`);
  }

  generateCsvFileName() {
    const event = this.panel.currentEvents.map((_event) => _event.name);
    const queryCode = this.panel.currentQuery.name.split(' ')[0];
    const time = this.panel.customTimeRange;
    let metrics = [];
    let playersOrTeams = [];
    if (this.panel.playerQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
      metrics = metrics.concat(this.panel.currentPlayerMetrics.map((metric) => metric.name));
      if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
        playersOrTeams = playersOrTeams.concat([this.panel.currentPlayers[0].name, this.panel.currentPlayers[1].name]);
      } else {
        playersOrTeams.push(this.panel.currentPlayers[0].name);
      }
    } else {
      metrics = metrics.concat(this.panel.currentTeamMetrics.map((metric) => metric.name));
      if (this.panel.comparisonQueryCodes.indexOf(this.panel.currentQuery.code) > -1) {
        playersOrTeams = playersOrTeams.concat([this.panel.currentTeams[0].name, this.panel.currentTeams[1].name]);
      } else {
        playersOrTeams.push(this.panel.currentTeams[0].name);
      }
    }
    return `${event}-${queryCode}-${metrics}-${playersOrTeams}-${time.startTime}-${time.endTime}`;
  }
}

QueryCtrl.templateUrl = 'module.html';
