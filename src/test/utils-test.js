/**
 * Created by viacheslav on 12/1/17.
 */
import chai from 'chai';
import {getEvents, getPlayerSensorData, getTeamSensorData} from './test-data';
import {calculateMinStartAndMaxEnd, distinctTeams, distinctPlayers, normalizePlayerResponse, normalizeTeamResponse}
from '../utils';

const assert = chai.assert;

describe('Test untils methods', () => {
  const events = getEvents();
  const playerSensorData = getPlayerSensorData();
  const teamSensorData = getTeamSensorData();

  describe('calculateMinStartAndMaxEnd', () => {
    it('The first and the second events', () => {
      const eventsList = [events[0], events[1]];
      const timeRange = calculateMinStartAndMaxEnd(eventsList);
      const expectedTimeRange = {
        startTime: '2017-10-09T09:02:00.000Z',
        endTime: '2018-01-31T10:03:00.000Z'
      };
      assert.equal(timeRange.startTime, expectedTimeRange.startTime);
      assert.equal(timeRange.endTime, expectedTimeRange.endTime);
    });

    it('The first and the third events', () => {
      const eventsList = [events[0], events[2]];
      const timeRange = calculateMinStartAndMaxEnd(eventsList);
      const expectedTimeRange = {
        startTime: '2017-11-08T08:05:00.000Z',
        endTime: '2017-11-14T23:35:00.000Z'
      };
      assert.equal(timeRange.startTime, expectedTimeRange.startTime);
      assert.equal(timeRange.endTime, expectedTimeRange.endTime);
    });

    it('All three events', () => {
      const timeRange = calculateMinStartAndMaxEnd(events);
      const expectedTimeRange = {
        startTime: '2017-10-09T09:02:00.000Z',
        endTime: '2018-01-31T10:03:00.000Z'
      };
      assert.equal(timeRange.startTime, expectedTimeRange.startTime);
      assert.equal(timeRange.endTime, expectedTimeRange.endTime);
    });
  });

  describe('distinctTeams', () => {
    const teamsEvent1 = [events[0].homeTeam, events[0].awayTeam];
    const teamsEvent2 = [events[1].homeTeam, events[1].awayTeam];
    const teamsEvent3 = [events[2].homeTeam, events[2].awayTeam];

    it('All home and away teams from all events', () => {
      const teams = teamsEvent1.concat(teamsEvent2.concat(teamsEvent3));
      const distinctTeamsList = distinctTeams(teams);
      assert.equal(distinctTeamsList.length, 5);
    });

    it('All home and away teams from the first and the third events', () => {
      const teams = teamsEvent1.concat(teamsEvent3);
      const distinctTeamsList = distinctTeams(teams);
      assert.equal(distinctTeamsList.length, 3);
    });

    it('All home and away teams from the second and the third events', () => {
      const teams = teamsEvent2.concat(teamsEvent3);
      const distinctTeamsList = distinctTeams(teams);
      assert.equal(distinctTeamsList.length, 4);
    });
  });

  describe('distinctPlayers', () => {
    const playersEvent1 = events[0].homeTeam.players.concat(events[0].awayTeam.players);
    const playersEvent2 = events[1].homeTeam.players.concat(events[1].awayTeam.players);
    const playersEvent3 = events[2].homeTeam.players.concat(events[2].awayTeam.players);

    it('All players from all events', () => {
      const players = playersEvent1.concat(playersEvent2.concat(playersEvent3));
      const distinctPlayersList = distinctPlayers(players);
      assert.equal(distinctPlayersList.length, 17);
    });

    it('All players from Event1 and Event2', () => {
      const players = playersEvent1.concat(playersEvent2);
      const distinctPlayersList = distinctPlayers(players);
      assert.equal(distinctPlayersList.length, 12);
    });

    it('All players from Event1 and Event3', () => {
      const players = playersEvent1.concat(playersEvent3);
      const distinctPlayersList = distinctPlayers(players);
      assert.equal(distinctPlayersList.length, 10);
    });
  });

  describe('normalizePlayerResponse', () => {
    const normalizedPlayerResponse = normalizePlayerResponse(playerSensorData);

    it('Normalized response contains 10 elements', () => {
      assert.equal(normalizedPlayerResponse.length, 10);
    });

    it('Normalized response contains 3 energy elements', () => {
      const energyElements = normalizedPlayerResponse.filter((element) => element.metric === 'Energy');
      assert.equal(energyElements.length, 3);
    });

    it('Normalized response contains 2 explosion elements', () => {
      const explosionElements = normalizedPlayerResponse.filter((element) => element.metric === 'Explosion');
      assert.equal(explosionElements.length, 2);
    });

    it('Normalized response contains 5 step elements', () => {
      const stepElements = normalizedPlayerResponse.filter((element) => element.metric === 'Step');
      assert.equal(stepElements.length, 5);
    });
  });

  describe('normalizeTeamResponse', () => {
    const normalizedTeamResponse = normalizeTeamResponse(teamSensorData);

    it('Normalized response contains 12 elements', () => {
      assert.equal(normalizedTeamResponse.length, 12);
    });

    it('Normalized response contains 5 noise elements', () => {
      const noiseLevelElements = normalizedTeamResponse.filter((element) => element.metric === 'Noise level');
      assert.equal(noiseLevelElements.length, 5);
    });

    it('Normalized response contains 3 fan attitudes elements', () => {
      const noiseLevelElements = normalizedTeamResponse.filter((element) => element.metric === 'Fan attitudes');
      assert.equal(noiseLevelElements.length, 3);
    });

    it('Normalized response contains 4 audio classes elements', () => {
      const noiseLevelElements = normalizedTeamResponse.filter((element) => element.metric === 'Audio classes');
      assert.equal(noiseLevelElements.length, 4);
    });
  });
});
