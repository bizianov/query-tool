/**
 * Created by viacheslav on 12/1/17.
 */
const getEvents = () => {
  return [
    {
      id: 'appyr:el_E2017_50',
      shortName: 'TELIST',
      duration: {
        start: '2017-11-13T08:05:00.000Z',
        end: '2017-11-14T23:35:00.000Z'
      },
      homeTeam: {
        id: 'sportsr:el_team_TEL',
        name: 'Maccabi FOX Tel Aviv',
        players: [
          {
            id: 'sportsr:el_team_mbr_005112',
            name: 'JACKSON, PIERRE'
          },
          {
            id: 'sportsr:el_team_mbr_005094',
            name: 'THOMAS, DESHAUN'
          }
        ]
      },
      awayTeam: {
        id: 'sportsr:el_team_IST',
        name: 'Anadolu Efes Istanbul',
        players: [
          {
            id: 'sportsr:el_team_mbr_004170',
            name: 'BATUK, BIRKAN'
          },
          {
            id: 'sportsr:el_team_mbr_007991',
            name: 'MUSTAFA, MUHAYMIN'
          },
          {
            id: 'sportsr:el_team_mbr_004094',
            name: 'DEMIR, BERK'
          }
        ]
      }
    },
    {
      id: 'appyr:5140437b-5dd0-4457-8cb9-974b629c83a6',
      shortName: 'HeedTest1',
      duration: {
        start: '2017-10-09T09:02:00.000Z',
        end: '2018-01-31T10:03:00.000Z'
      },
      homeTeam: {
        id: 'appyr:c5ee2df2-f3e9-44df-9e79-1a15d1f11581',
        name: 'Demo Team 1',
        players: [
          {
            id: 'appyr:07519560-357e-4a55-b480-6cc569c54eca',
            name: 'Demo Player 4'
          },
          {
            id: 'appyr:407bbf19-1d23-46c2-9d5d-5d8a464bc2eb',
            name: 'Demo Player 1'
          },
          {
            id: 'appyr:06a15119-bf77-40d2-9e62-45a620611306',
            name: 'Demo Player 3'
          }
        ]
      },
      awayTeam: {
        id: 'appyr:67957739-6cff-40e1-877b-8ee1d28d4641',
        name: 'Demo Team A',
        players: [
          {
            id: 'appyr:d94c082d-0bec-42b4-8eac-9aa67221e370',
            name: 'Demo Player A'
          },
          {
            id: 'appyr:c7346d2d-f1c0-4544-bc34-e34d1af66a3f',
            name: 'Demo Player 2'
          },
          {
            id: 'appyr:45161062-5180-4a56-89e9-b3a437f7ade5',
            name: 'Demo Player C'
          },
          {
            id: 'appyr:a0eb57ea-06bf-4861-95de-56da7513e341',
            name: 'Demo Player D'
          }
        ]
      }
    },
    {
      id: 'appyr:el_E2017_44',
      shortName: 'TELMAD',
      duration: {
        start: '2017-11-08T08:05:00.000Z',
        end: '2017-11-09T23:35:00.000Z'
      },
      homeTeam: {
        id: 'sportsr:el_team_TEL',
        name: 'Maccabi FOX Tel Aviv',
        players: [
          {
            id: 'sportsr:el_team_mbr_005112',
            name: 'JACKSON, PIERRE'
          },
          {
            id: 'sportsr:el_team_mbr_005094',
            name: 'THOMAS, DESHAUN'
          },
          {
            id: 'sportsr:el_team_mbr_004974',
            name: 'ROLL, MICHAEL'
          }
        ]
      },
      awayTeam: {
        id: 'sportsr:el_team_MAD',
        name: 'Real Madrid',
        players: [
          {
            id: 'sportsr:el_team_mbr_005836',
            name: 'THOMPKINS, TREY'
          },
          {
            id: 'sportsr:el_team_mbr_008038',
            name: 'RANDLE, CHASSON'
          },
          {
            id: 'sportsr:el_team_mbr_005368',
            name: 'YUSTA, SANTI'
          },
          {
            id: 'sportsr:el_team_mbr_AAX',
            name: 'REYES, FELIPE'
          }
        ]
      }
    }
  ];
};

const getPlayerSensorData = () => {
  return [
    {
      'name': 'TODOROVIC, MARKO',
      'label': 'TODOROVIC, MARKO',
      'heartrates': [],
      'pressures': [],
      'steps': [
        {
          'impact': 0,
          'speed': 0,
          'time': '2017-11-24T15:20:03.000Z'
        },
        {
          'impact': 0,
          'speed': 0,
          'time': '2017-11-24T15:20:04.000Z'
        },
        {
          'impact': 0,
          'speed': 0,
          'time': '2017-11-24T15:20:05.000Z'
        },
        {
          'impact': 0,
          'speed': 0,
          'time': '2017-11-24T15:20:06.000Z'
        },
        {
          'impact': 0,
          'speed': 0,
          'time': '2017-11-24T15:20:07.000Z'
        }
      ],
      'explosions': [
        {
          'index': 4.416,
          'time': '2017-11-24T15:19:59.000Z'
        },
        {
          'index': 6.011333333333333,
          'time': '2017-11-24T15:20:00.000Z'
        }
      ],
      'energy': [
        {
          'energy': 2.14083562776833,
          'time': '2017-11-24T15:19:59.000Z'
        },
        {
          'energy': 2.9177348112679518,
          'time': '2017-11-24T15:20:00.000Z'
        },
        {
          'energy': 2.25536342296014,
          'time': '2017-11-24T15:20:01.000Z'
        }
      ],
      'jumps': []
    }
  ];
};

const getTeamSensorData = () => {
  return [
    {
      'shortName': null,
      'name': 'Fenerbahce Dogus Istanbul',
      'label': 'Fenerbahce Dogus Istanbul',
      'noiseLevels': [
        {
          'time': '2017-11-23T06:45:00.000Z',
          'decibel': '64.75776104947887'
        },
        {
          'time': '2017-11-23T06:45:00.000Z',
          'decibel': '65.01285219537128'
        },
        {
          'time': '2017-11-23T06:45:01.000Z',
          'decibel': '64.62186574409803'
        },
        {
          'time': '2017-11-23T06:45:01.000Z',
          'decibel': '64.9140517255544'
        },
        {
          'time': '2017-11-23T06:45:02.000Z',
          'decibel': '64.74933761343375'
        }
      ],
      'fanAttitudes': [
        {
          'noiseLevel': 67.726579724697,
          'noiseIndex': 0.5794934793522752,
          'class': 'background',
          'teamName': 'nan',
          'attitude': 0,
          'time': '2017-11-24T13:59:46.000Z'
        },
        {
          'noiseLevel': 67.28003518731404,
          'noiseIndex': 0.5460026390485531,
          'class': 'background',
          'teamName': 'nan',
          'attitude': 0,
          'time': '2017-11-24T13:59:47.000Z'
        },
        {
          'noiseLevel': 66.64536284062973,
          'noiseIndex': 0.4984022130472297,
          'class': 'background',
          'teamName': 'nan',
          'attitude': 0,
          'time': '2017-11-24T13:59:48.000Z'
        }
      ],
      'audioClasses': [
        {
          'stop': null,
          'start': '2017-11-23T06:45:00.000Z',
          'class': 'background'
        },
        {
          'stop': null,
          'start': '2017-11-23T06:45:00.000Z',
          'class': 'background'
        },
        {
          'stop': null,
          'start': '2017-11-23T06:45:01.000Z',
          'class': 'background'
        },
        {
          'stop': null,
          'start': '2017-11-23T06:45:01.000Z',
          'class': 'background'
        }
      ]
    }
  ];
};

export {getEvents, getPlayerSensorData, getTeamSensorData};
