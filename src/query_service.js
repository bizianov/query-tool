/**
 * Created by viacheslav on 11/28/17.
 */
import {getEventsListWithPlayers, getAllSensorDataByPlayerNameInTimeRange, getAllSensorDataByTeamNameInTimeRange,
    getPlayerAverageDateInTimeRange, getTeamAverageDateInTimeRange, getPlayerSumDataInTimeRange,
    getTeamSumDataInTimeRange} from './queries';

export class QueryService {
  constructor($http, url, username, password) {
    this.$http = $http;
    this.url = url;
    this.username = username;
    this.password = password;
    this.queryMapping = {
      0: getAllSensorDataByPlayerNameInTimeRange,
      1: getAllSensorDataByTeamNameInTimeRange,
      2: getPlayerAverageDateInTimeRange,
      3: getTeamAverageDateInTimeRange,
      6: getPlayerSumDataInTimeRange,
      7: getTeamSumDataInTimeRange,
      100: getEventsListWithPlayers,
    };
  }

  executeQuery(query, variables) {
    console.log('Variables: ', variables);
    const url = `http://localhost:1337/${this.url}`;
    const convertedAuth = window.btoa(this.username + ':' + this.password).toString('base64');
    return this.$http({
      method: 'POST',
      url: url,
      data: JSON.stringify({
        query: this.queryMapping[query.code],
        variables: variables,
      }),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Basic ' + convertedAuth,
      }
    }).then((response) => {
      console.log('Response: ', response);
      return response;
    });
  }

  getEventsListWithPlayers(variables) {
    return this.executeQuery({name: 'Events list with players', code: 100}, variables);
  }
}
