/**
 * Created by viacheslav on 12/7/17.
 */

export class CsvService {

  constructor(csvDelimiter) {
    this.csvDelimiter = csvDelimiter;
  }

  exportTableToCSV(filename) {
    const csv = [];
    const rows = document.querySelectorAll('table tr');

    for (let index = 0; index < rows.length; index++) {
      const row = [];
      const cols = rows[index].querySelectorAll('td, th');

      for (let jIndex = 0; jIndex < cols.length; jIndex++) {
        row.push(cols[jIndex].innerText);
      }

      csv.push(row.join(this.csvDelimiter));
    }

    // Download CSV file
    this.downloadCSV(csv.join('\n'), filename);
  }

  downloadCSV(csv, filename) {
    // CSV file
    const csvFile = new Blob([csv], {type: 'text/csv'});

    // Download link
    const downloadLink = document.createElement('a');

    // File name
    downloadLink.download = filename;

    // Create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);

    // Hide download link
    downloadLink.style.display = 'none';

    // Add the link to DOM
    document.body.appendChild(downloadLink);

    // Click download link
    downloadLink.click();
  }
}
